export const ZhHansMsg = {};

//科易互动机器人-坦克小车
ZhHansMsg.tank_front='前进';
ZhHansMsg.tank_back='后退';
ZhHansMsg.tank_left='左边';
ZhHansMsg.tank_turn_left='左转弯';
ZhHansMsg.tank_right='右边';
ZhHansMsg.tank_turn_right='右转弯';
ZhHansMsg.tank_stop='停止';
ZhHansMsg.tank_speed='速度值';

ZhHansMsg.tank_choose='选择';
ZhHansMsg.left_light_sensor='左光线传感器';
ZhHansMsg.right_light_sensor='右光线传感器';

ZhHansMsg.tank_L_ir_a='左红外避障';
ZhHansMsg.tank_R_ir_a='右红外避障';
ZhHansMsg.tank_L_track='左循迹传感器';
ZhHansMsg.tank_C_track='中循迹传感器';
ZhHansMsg.tank_R_track='右循迹传感器';

ZhHansMsg.tank_sr01='超声波';
ZhHansMsg.tank_buzzer='蜂鸣器';
ZhHansMsg.tank_notone = '关闭蜂鸣器';
ZhHansMsg.tank_play_music = '播放乐曲';
ZhHansMsg.tank_Ode_to_joy = '欢乐颂';
ZhHansMsg.tank_birthday = '生日快乐';
ZhHansMsg.tank_fre='频率';

ZhHansMsg.MIXLY_Tank_SERVO = '舵机';
ZhHansMsg.MIXLY_Tank_matrix = '点阵初始化';

ZhHansMsg.tank_ir_R='红外接收模块';
ZhHansMsg.tank_ir_RD='红外接收数据';
ZhHansMsg.tank_Bluetooth='蓝牙BLE模块';
ZhHansMsg.tank_Bluetooth_rec='蓝牙接收数据';

ZhHansMsg.tank_on_off='电平为';
ZhHansMsg.tank_high='高';
ZhHansMsg.tank_low='低';

//小乌龟车
ZhHansMsg.turtle_front='前进';
ZhHansMsg.turtle_back='后退';
ZhHansMsg.turtle_left='左边';
ZhHansMsg.turtle_turn_left='左转弯';
ZhHansMsg.turtle_right='右边';
ZhHansMsg.turtle_turn_right='右转弯';
ZhHansMsg.turtle_stop='停止';
ZhHansMsg.turtle_speed='速度值';

ZhHansMsg.turtle_L_ir_a='左红外避障';
ZhHansMsg.turtle_R_ir_a='右红外避障';
ZhHansMsg.turtle_L_track='左循迹传感器';
ZhHansMsg.turtle_C_track='中循迹传感器';
ZhHansMsg.turtle_R_track='右循迹传感器';

ZhHansMsg.turtle_sr01='超声波';
ZhHansMsg.turtle_buzzer='蜂鸣器';
ZhHansMsg.turtle_notone = '关闭蜂鸣器';
ZhHansMsg.turtle_play_music = '播放乐曲';
ZhHansMsg.turtle_Ode_to_joy = '欢乐颂';
ZhHansMsg.turtle_birthday = '生日快乐';
ZhHansMsg.turtle_fre='频率';

ZhHansMsg.MIXLY_turtle_SERVO = '舵机';
ZhHansMsg.MIXLY_turtle_matrix = '点阵初始化';

ZhHansMsg.turtle_ir_R='红外接收模块';
ZhHansMsg.turtle_ir_RD='红外接收数据';
ZhHansMsg.turtle_Bluetooth='蓝牙BLE模块';
ZhHansMsg.turtle_Bluetooth_rec='蓝牙接收数据';

ZhHansMsg.turtle_on_off='电平为';
ZhHansMsg.turtle_high='高';
ZhHansMsg.turtle_low='低';

//2wd桌面小车
ZhHansMsg.Desk_on_off='电平为';

ZhHansMsg.Desk_front='前进';
ZhHansMsg.Desk_back='后退';
ZhHansMsg.Desk_left='左边';
ZhHansMsg.Desk_turn_left='左转弯';
ZhHansMsg.Desk_right='右边';
ZhHansMsg.Desk_turn_right='右转弯';
ZhHansMsg.Desk_stop='停止';
ZhHansMsg.Desk_speed='速度值';

ZhHansMsg.Desk_L_ir_a='左红外避障';
ZhHansMsg.Desk_R_ir_a='右红外避障';
ZhHansMsg.Desk_L_track='左循迹传感器';
ZhHansMsg.Desk_C_track='中循迹传感器';
ZhHansMsg.Desk_R_track='右循迹传感器';

ZhHansMsg.Desk_sr01='超声波';
ZhHansMsg.Desk_buzzer='蜂鸣器';
ZhHansMsg.Desk_notone = '关闭蜂鸣器';
ZhHansMsg.Desk_play_music = '播放乐曲';
ZhHansMsg.Desk_Ode_to_joy = '欢乐颂';
ZhHansMsg.Desk_birthday = '生日快乐';
ZhHansMsg.Desk_fre='频率';
ZhHansMsg.Desk_ir_R='红外接收模块';
ZhHansMsg.Desk_ir_RD='红外接收数据';
ZhHansMsg.Desk_Bluetooth='蓝牙BLE模块';
ZhHansMsg.Desk_Bluetooth_rec='蓝牙接收数据';

//4wd小车
ZhHansMsg.Mecanumcar='麦轮小车';
ZhHansMsg.Car_state='小车状态为';
ZhHansMsg.ks4wd_front='前进';
ZhHansMsg.ks4wd_back='后退';
ZhHansMsg.ks4wd_left='左边';
ZhHansMsg.ks4wd_turn_left='左转弯';
ZhHansMsg.ks4wd_right='右边';
ZhHansMsg.ks4wd_turn_right='右转弯';
ZhHansMsg.ks4wd_stop='停止';
ZhHansMsg.ks4wd_speed='速度值';

ZhHansMsg.ks4wd_choose='选择';

ZhHansMsg.ks4wd_L_ir_a='左红外避障';
ZhHansMsg.ks4wd_R_ir_a='右红外避障';
ZhHansMsg.ks4wd_L_track='左循迹传感器';
ZhHansMsg.ks4wd_C_track='中循迹传感器';
ZhHansMsg.ks4wd_R_track='右循迹传感器';

ZhHansMsg.ks4wd_sr01='超声波';
ZhHansMsg.ks4wd_buzzer='蜂鸣器';
ZhHansMsg.ks4wd_notone = '关闭蜂鸣器';
ZhHansMsg.ks4wd_play_music = '播放乐曲';
ZhHansMsg.ks4wd_Ode_to_joy = '欢乐颂';
ZhHansMsg.ks4wd_birthday = '生日快乐';
ZhHansMsg.ks4wd_fre='频率';

ZhHansMsg.MIXLY_ks4wd_SERVO = '舵机';
ZhHansMsg.MIXLY_ks4wd_matrix = '点阵初始化';

ZhHansMsg.ks4wd_ir_R='红外接收模块';
ZhHansMsg.ks4wd_ir_RD='红外接收数据';
ZhHansMsg.ks4wd_Bluetooth='蓝牙模块';
ZhHansMsg.ks4wd_Bluetooth_rec='蓝牙接收数据';
ZhHansMsg.MIXLY_SERIAL_READSTR_UNTIL = '蓝牙读取字符串直到';

ZhHansMsg.ks4wd_on_off='电平为';
ZhHansMsg.ks4wd_high='高';
ZhHansMsg.ks4wd_low='低';

ZhHansMsg.ksCar_forward='前进';
ZhHansMsg.ksCar_backward='后退';
ZhHansMsg.ksCar_left='左旋转';
ZhHansMsg.ksCar_right='右旋转';


ZhHansMsg.MIXLY_brightness='亮度';
ZhHansMsg.MIXLY_init_brightness='初始亮度';
ZhHansMsg.MIXLY_TIME_FOR_CHANGE_COLOUR='七彩变换切换时间';
ZhHansMsg.MIXLY_TIME_FOR_LOOP_COLOUR='七彩循环切换时间';
ZhHansMsg.MIXLY_drawLine='点阵画直线';
ZhHansMsg.MIXLY_drawdrawCircle='点阵画圆';
ZhHansMsg.MIXLY_drawRect='点阵画矩形';
ZhHansMsg.MIXLY_drawRoundRect='点阵画圆角矩形';
ZhHansMsg.MIXLY_drawTriangle='点阵画三角形';
ZhHansMsg.MIXLY_Scroll_direction='点阵屏滚动文字方向';
ZhHansMsg.MIXLY_Preset_photo='点阵预设图案';

//OTTO_frog
ZhHansMsg.otto_init='初始化';
ZhHansMsg.otto_YL='调整左腿';
ZhHansMsg.otto_YR='调整右腿';
ZhHansMsg.otto_RL='调整左脚';
ZhHansMsg.otto_RR='调整右脚';
ZhHansMsg.otto_steps='步数';
ZhHansMsg.otto_speed='速度';
ZhHansMsg.otto_left_moonwalk='左太空步';
ZhHansMsg.otto_right_moonwalk='右太空步';
ZhHansMsg.otto_left_galop='左快步';
ZhHansMsg.otto_right_galop='右快步';
ZhHansMsg.otto_drunk='摆动';
ZhHansMsg.otto_noGravity='失重状态中';
ZhHansMsg.otto_crusaito='滑步';
ZhHansMsg.otto_friction_pace='摩擦摩擦';
ZhHansMsg.otto_jump='跳跳跳。。。';
ZhHansMsg.otto_flapping='摇摆运动';
ZhHansMsg.otto_swing='游泳姿势';
ZhHansMsg.otto_goingUp='慢慢踮起脚';
ZhHansMsg.otto_front='前进';
ZhHansMsg.otto_back='后退';
ZhHansMsg.otto_left='左旋转';
ZhHansMsg.otto_turn_left='左转弯';
ZhHansMsg.otto_right='右旋转';
ZhHansMsg.otto_turn_right='右转弯';
ZhHansMsg.otto_stop='保持站立';
ZhHansMsg.otto_speed='速度值';
ZhHansMsg.otto_servo='舵机';
ZhHansMsg.otto_angle='角度';
ZhHansMsg.otto_sr01='超声波';
ZhHansMsg.otto_Bluetooth='蓝牙BLE模块';
ZhHansMsg.otto_Bluetooth_rec='蓝牙接收数据';


ZhHansMsg.Kids_anologWrite = 'PWM模拟输出';
ZhHansMsg.Kids_value = '赋值为';

ZhHansMsg.Kids_iic = '管脚：SDA# A4, SCL# A5';
ZhHansMsg.Kids_rot = '按钮管脚';
ZhHansMsg.Kids_rot_count = '计数变量';
ZhHansMsg.Kids_bits = '字符串';
ZhHansMsg.Kids_pin = '管脚';

ZhHansMsg.Kids_iic_pin = '管脚SDA:A4,SCL:A5';
ZhHansMsg.Kids_lcd_p = '液晶显示屏';
ZhHansMsg.Kids_shilihua = '实例化名称';
ZhHansMsg.Kids_size = '字体大小';
ZhHansMsg.Kids_printcount = '显示数字';

ZhHansMsg.ke_string = '显示字符';

ZhHansMsg.Kids_lcd_left = '液晶显示屏往左滚动';
ZhHansMsg.Kids_lcd_right = '液晶显示屏往右滚动';



ZhHansMsg.Kids_ON = '高';
ZhHansMsg.Kids_OFF = '低';
ZhHansMsg.Kids_anologWrite = 'PWM模拟输出';

ZhHansMsg.MIXLY_SETUP='初始化';  ////////////////
ZhHansMsg.MIXLY_LCD_ADDRESS='设备地址'; /////////
ZhHansMsg.MIXLY_LCD_PRINT1='打印第一行';  /////////
ZhHansMsg.MIXLY_LCD_PRINT2='打印第二行'; ///////////
ZhHansMsg.MIXLY_LCD_ROW='在第'; /////
ZhHansMsg.MIXLY_LCD_COLUMN='行第'; /////////
ZhHansMsg.MIXLY_LCD_PRINT='列打印'; ////
ZhHansMsg.MIXLY_LCD_STAT_ON='开';  /////
ZhHansMsg.MIXLY_LCD_STAT_OFF='关';  /////
ZhHansMsg.MIXLY_LCD_STAT_CURSOR='有光标';  ////
ZhHansMsg.MIXLY_LCD_STAT_NOCURSOR='无光标';  ////
ZhHansMsg.MIXLY_LCD_STAT_BLINK='闪烁';  ////
ZhHansMsg.MIXLY_LCD_STAT_NOBLINK='不闪烁'; ///
ZhHansMsg.MIXLY_LCD_STAT_CLEAR='清屏';  ///
ZhHansMsg.MIXLY_LCD_NOBACKLIGHT='关闭背光'; ///
ZhHansMsg.MIXLY_LCD_BACKLIGHT='打开背光'; ///



//ZhHansMsg.MIXLY_ke_kzsc = '控制输出';

ZhHansMsg.MIXLY_ke_Count='灯号';

ZhHansMsg.MIXLY_ke_YEAR = '年';
ZhHansMsg.MIXLY_ke_MONTH = '月';
ZhHansMsg.MIXLY_ke_DAY = '日';
ZhHansMsg.MIXLY_ke_HOUR = '时';
ZhHansMsg.MIXLY_ke_MINUTE = '分';
ZhHansMsg.MIXLY_ke_SECOND = '秒';
ZhHansMsg.MIXLY_ke_WEEK = '周';

ZhHansMsg.MIXLY_ke_angle = '角度';

ZhHansMsg.kids_Ode_to_joy = "圣诞歌";
ZhHansMsg.kids_birthday = "生日快乐";

ZhHansMsg.kids_tone = "音调";
ZhHansMsg.kids_beat = "节拍";
ZhHansMsg.kids_play_tone = "播放乐曲";
ZhHansMsg.kids_notone = "关闭蜂鸣器";

ZhHansMsg.kids_ADkey = "7位按键模块";


//keyestudio传感器 、、、、、、、、、、、、、、、、、、、、、、、、、、、\\\\\\\\\\\

ZhHansMsg.KS_LED3wd='LED 3W';
ZhHansMsg.KS_LED_W='白色LED';
ZhHansMsg.KS_LED_R='红色LED';
ZhHansMsg.KS_LED_G='绿色LED';
ZhHansMsg.KS_LED_B='蓝色LED';
ZhHansMsg.KS_LED_Y='黄色LED';
ZhHansMsg.MIXLY_KS_BUZZER1='有源蜂鸣器';
ZhHansMsg.MIXLY_KS_BUZZER2='无源蜂鸣器';
ZhHansMsg.ks_tone='音调';
ZhHansMsg.ks_beat='节拍';
ZhHansMsg.ks_music='播放乐曲';
ZhHansMsg.ks_Ode_to_joy='圣诞歌';
ZhHansMsg.ks_birthday='生日快乐';
ZhHansMsg.ks_tetris='俄罗斯方块';
ZhHansMsg.ks_star_war='星球大战';
ZhHansMsg.ks_super_mario='超级玛丽';
ZhHansMsg.ks_christmas='圣诞歌';
ZhHansMsg.ks_notone1='关闭蜂鸣器';

ZhHansMsg.ks_MQ_d='MQ-2可燃气体传感器_数字';
ZhHansMsg.ks_MQ_a='MQ-2可燃气体传感器_模拟';

ZhHansMsg.MIXLY_KS_RELAY='继电器';
ZhHansMsg.MIXLY_KS_MOTOR='小风扇';

ZhHansMsg.Ks_ON='高';
ZhHansMsg.Ks_OFF='低';

ZhHansMsg.MIXLY_KS_SERVO='舵机';
ZhHansMsg.MIXLY_KS_2812RGB='2812RGB';

ZhHansMsg.MIXLY_KS_IR_G='人体红外传感器';
ZhHansMsg.MIXLY_KS_FLAME='火焰传感器_数字值';
ZhHansMsg.MIXLY_KS_FLAME_a='火焰传感器_模拟值';
ZhHansMsg.MIXLY_KS_HALL='霍尔传感器';
ZhHansMsg.MIXLY_KS_CRASH='碰撞传感器';
ZhHansMsg.MIXLY_KS_BUTTON='按键';
ZhHansMsg.MIXLY_KS_TUOCH='电容触摸';
ZhHansMsg.MIXLY_KS_KNOCK='敲击模块';
ZhHansMsg.MIXLY_KS_TILT='倾斜模块';
ZhHansMsg.MIXLY_KS_SHAKE='振动模块';
ZhHansMsg.MIXLY_KS_REED_S='干簧管模块';
ZhHansMsg.MIXLY_KS_TRACK='循迹模块';
ZhHansMsg.MIXLY_KS_AVOID='避障模块';
ZhHansMsg.MIXLY_KS_LIGHT_B='光折断模块';

ZhHansMsg.MIXLY_KS_ANALOG_T='模拟温度传感器';
ZhHansMsg.MIXLY_KS_SOUND='声音传感器';
ZhHansMsg.KS_LIGHT='光敏传感器';
ZhHansMsg.MIXLY_KS_WATER='水位传感器';
ZhHansMsg.KS_SOIL='土壤湿度传感器';
ZhHansMsg.MIXLY_KS_POTENTIOMETER='旋转电位器';
ZhHansMsg.MIXLY_KS_LM35='LM35温度传感器';
ZhHansMsg.MIXLY_KS_SLIDE_POTENTIOMETER='滑动电位器';
ZhHansMsg.MIXLY_KS_TEMT6000='TEMT6000环境光';
ZhHansMsg.KS_STEAM='水滴传感器';
ZhHansMsg.MIXLY_KS_FILM_P='薄膜压力传感器';
ZhHansMsg.MIXLY_KS_JOYSTICK='遥杆模块';
ZhHansMsg.MIXLY_KS_SMOKE='烟雾传感器';
ZhHansMsg.MIXLY_KS_ALCOHOL='酒精传感器';
ZhHansMsg.MIXLY_KS_MQ135='MQ135空气质量';

ZhHansMsg.MIXLY_KS_18B20='18B20温度模块';
ZhHansMsg.MIXLY_KS_RT='温度';

ZhHansMsg.MIXLY_KS_DHT11='温湿度模块';

ZhHansMsg.MIXLY_KS_BMP180='BMP180高度计模块';
ZhHansMsg.MIXLY_KS_T='温度';
ZhHansMsg.MIXLY_KS_QY='大气压';
ZhHansMsg.MIXLY_KS_H='高度';

ZhHansMsg.KS_SR01='超声波模块';
ZhHansMsg.MIXLY_KS_3231='DS3231时钟';


ZhHansMsg.MIXLY_KS_YEAR = '年';
ZhHansMsg.MIXLY_KS_MONTH = '月';
ZhHansMsg.MIXLY_KS_DAY = '天';
ZhHansMsg.MIXLY_KS_TEXT = '周';
ZhHansMsg.MIXLY_KS_HOUR = '时';
ZhHansMsg.MIXLY_KS_MINUTE = '分';
ZhHansMsg.MIXLY_KS_SECOND = '秒';
ZhHansMsg.MIXLY_KS_GET = '获取DS3231时钟时间';


ZhHansMsg.MIXLY_KS_OLED='OLED模块';

ZhHansMsg.MIXLY_SETUP='初始化';  ////////////////
ZhHansMsg.MIXLY_LCD_ADDRESS='设备地址'; /////////
ZhHansMsg.MIXLY_LCD_PRINT1='打印第一行';  /////////
ZhHansMsg.MIXLY_LCD_PRINT2='打印第二行'; ///////////
ZhHansMsg.MIXLY_LCD_ROW='在第'; /////
ZhHansMsg.MIXLY_LCD_COLUMN='行第'; /////////
ZhHansMsg.MIXLY_LCD_PRINT='列打印'; ////
ZhHansMsg.MIXLY_LCD_STAT_ON='开';  /////
ZhHansMsg.MIXLY_LCD_STAT_OFF='关';  /////
ZhHansMsg.MIXLY_LCD_STAT_CURSOR='有光标';  ////
ZhHansMsg.MIXLY_LCD_STAT_NOCURSOR='无光标';  ////
ZhHansMsg.MIXLY_LCD_STAT_BLINK='闪烁';  ////
ZhHansMsg.MIXLY_LCD_STAT_NOBLINK='不闪烁'; ///
ZhHansMsg.MIXLY_LCD_STAT_CLEAR='清屏';  ///
ZhHansMsg.MIXLY_LCD_NOBACKLIGHT='关闭背光'; ///
ZhHansMsg.MIXLY_LCD_BACKLIGHT='打开背光'; ///
ZhHansMsg.MIXLY_DHT11_H='获取湿度';    /////////////
ZhHansMsg.MIXLY_DHT11_T='获取温度';     ////////////
ZhHansMsg.MIXLY_DHT11_H='获取湿度';    /////////////
ZhHansMsg.MIXLY_DHT11_T='获取温度';     ////////////
ZhHansMsg.KS_ADXL345='三轴加速度计传感器_ADXL345';
ZhHansMsg.MIXLY_ADXL345_X='X轴加速度'; ///
ZhHansMsg.MIXLY_ADXL345_Y='Y轴加速度'; ///
ZhHansMsg.MIXLY_ADXL345_Z='Z轴加速度'; ///
ZhHansMsg.MIXLY_ADXL345_XA='X轴角度';  ///
ZhHansMsg.MIXLY_ADXL345_YA='Y轴角度';  ///

ZhHansMsg.MIXLY_KS_1602LCD='IIC1602LCD';
ZhHansMsg.MIXLY_KS_2004LCD='IIC2004LCD';

ZhHansMsg.Ks_iic_pin = '管脚SDA:A4,SCL:A5';
ZhHansMsg.Ks_lcd_p = '液晶显示屏';
ZhHansMsg.Ks_shilihua = '实例化名称';
ZhHansMsg.Ks_size = '字体大小';
ZhHansMsg.Ks_printcount = '显示数字';

ZhHansMsg.Ks_string = '显示字符';

ZhHansMsg.Ks_lcd_left = '液晶显示屏往左滚动';
ZhHansMsg.Ks_lcd_right = '液晶显示屏往右滚动';

ZhHansMsg.MIXLY_KS_MATRIX='8*8点阵';
ZhHansMsg.MIXLY_KS_TM1637='4位8段数码管';
ZhHansMsg.MIXLY_KS_ws='位数';
ZhHansMsg.MIXLY_KS_begin='显示的位置';
ZhHansMsg.MIXLY_KS_fill0='是否补充0';
ZhHansMsg.MIXLY_KS_light='亮度0~7';
ZhHansMsg.MIXLY_KS_XY='显或隐';
ZhHansMsg.MIXLY_KS_L='左边';
ZhHansMsg.MIXLY_KS_R='右边';
ZhHansMsg.MIXLY_KS_MH='冒号';
ZhHansMsg.MIXLY_KS_one='第一行';
ZhHansMsg.MIXLY_KS_two='第二行';
ZhHansMsg.MIXLY_KS_three='第三行';
ZhHansMsg.MIXLY_KS_four='第四行';
ZhHansMsg.MIXLY_KS_clear='             是否清屏:';

ZhHansMsg.MIXLY_KS_value='数值';

ZhHansMsg.MIXLY_KS_IR_E='红外发射模块';
ZhHansMsg.MIXLY_KS_IR_R='红外接收模块';
ZhHansMsg.MIXLY_KS_W5100='W5100以太网模块';
ZhHansMsg.KS_BLUETOOTH='蓝牙模块';
ZhHansMsg.MIXLY_KS_rec='接收到的信号';

ZhHansMsg.MIXLY_KS_kzsc = '控制输出';

ZhHansMsg.MIXLY_KS_Count='灯号';

ZhHansMsg.ks_test_V='电压传感器';
ZhHansMsg.ks_test_A='电流传感器';

ZhHansMsg.MIXLY_KS_FLAME_a='火焰传感器_模拟值';
ZhHansMsg.joys_btn='摇杆按钮';
ZhHansMsg.hwwd='非接触式红外传感器,引脚为 SDA : A4 , SCL : A5';
ZhHansMsg.color_sensor='颜色传感器';
ZhHansMsg.RED_VAL='红色值';
ZhHansMsg.GREEN_VAL='绿色值';
ZhHansMsg.BLUE_VAL='蓝色值';

ZhHansMsg.matrix16and8_init='点阵16*8初始化';
ZhHansMsg.matrix16and8='点阵16*8自定义图';
ZhHansMsg.matrix16and8_image='点阵16*8图';

ZhHansMsg.Ultraviolet='紫外线传感器';
ZhHansMsg.WaterTurbidity='水浑浊度';
ZhHansMsg.C2H5O='有害气体传感器';
ZhHansMsg.CeramicVibration='压电陶瓷震动传感器';
ZhHansMsg.DustSensor='粉尘传感器';

ZhHansMsg.MMA8452 = '三轴加速度计_MMA8452';
ZhHansMsg.SHT31 = '温湿度传感器_SHT31';

ZhHansMsg.ks_neopixel = 'RGB灯 neopixel';
ZhHansMsg.ks_neopixel_show = 'neopixel show';
ZhHansMsg.ks_TM1637_INIT ='四位数码管_TM1637 初始化';
ZhHansMsg.ks_4DIGITDISPLAY = '四位数码管_TM1637 显示';
ZhHansMsg.ks_4DIGITDISPLAY_time = '四位数码管_TM1637 显示时间';
ZhHansMsg.ks_4DIGITDISPLAY_brightness = '四位数码管_TM1637 亮度';
ZhHansMsg.ks_4DIGITDISPLAY_clean = '四位数码管_TM1637 清屏';


ZhHansMsg.ks_SPI_recv = '接收到数据 获取寄存器数据';
ZhHansMsg.ks_SPI_run = '执行';
ZhHansMsg.ks_SPI_read = 'SPI从机获取寄存器数据';

export const ZhHansCatgories = {};