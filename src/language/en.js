export const EnMsg = {};

//坦克车
EnMsg.tank_front='front';
EnMsg.tank_back='back';
EnMsg.tank_left='left';
EnMsg.tank_turn_left='turn_left';
EnMsg.tank_right='right';
EnMsg.tank_turn_right='turn_right';
EnMsg.tank_stop='stop';
EnMsg.tank_speed='speed';
EnMsg.left_light_sensor='left_light_sensor';
EnMsg.right_light_sensor='right_light_sensor';
EnMsg.tank_choose='choose';

EnMsg.tank_L_ir_a='left_infrared_avoid';
EnMsg.tank_R_ir_a='right_infrared_avoid';
EnMsg.tank_L_track='left_tracking';
EnMsg.tank_C_track='center_tracking';
EnMsg.tank_R_track='right_tracking';

EnMsg.tank_sr01='ultrasonic';
EnMsg.tank_buzzer='buzzer';
EnMsg.tank_notone = 'No_Tone';
EnMsg.tank_play_music = 'play_music';
EnMsg.tank_Ode_to_joy = 'Ode_to_Joy';
EnMsg.tank_birthday = 'birthday';
EnMsg.tank_fre='frequency';

EnMsg.MIXLY_Tank_SERVO = 'servo';
EnMsg.MIXLY_Tank_matrix = 'Matrix_init';

EnMsg.tank_ir_R='infrared_module';
EnMsg.tank_ir_RD='infrared_receive';
EnMsg.tank_Bluetooth='BLE_module';
EnMsg.tank_Bluetooth_rec='BLE_receive';

EnMsg.tank_on_off='electrical level';
EnMsg.tank_high='HIGH';
EnMsg.tank_low='LOW';


//乌龟车
EnMsg.turtle_front='front';
EnMsg.turtle_back='back';
EnMsg.turtle_left='left';
EnMsg.turtle_turn_left='turn_left';
EnMsg.turtle_right='right';
EnMsg.turtle_turn_right='turn_right';
EnMsg.turtle_stop='stop';
EnMsg.turtle_speed='speed';

EnMsg.turtle_L_ir_a='left_infrared_avoid';
EnMsg.turtle_R_ir_a='right_infrared_avoid';
EnMsg.turtle_L_track='left_tracking';
EnMsg.turtle_C_track='center_tracking';
EnMsg.turtle_R_track='right_tracking';

EnMsg.turtle_sr01='ultrasonic';
EnMsg.turtle_buzzer='buzzer';
EnMsg.turtle_notone = 'No_Tone';
EnMsg.turtle_play_music = 'play_music';
EnMsg.turtle_Ode_to_joy = 'Ode_to_Joy';
EnMsg.turtle_birthday = 'birthday';
EnMsg.turtle_fre='frequency';

EnMsg.MIXLY_turtle_SERVO = 'servo';
EnMsg.MIXLY_turtle_matrix = 'Matrix_init';

EnMsg.turtle_ir_R='infrared_module';
EnMsg.turtle_ir_RD='infrared_receive';
EnMsg.turtle_Bluetooth='BLE_module';
EnMsg.turtle_Bluetooth_rec='BLE_receive';

EnMsg.turtle_on_off='electrical level';
EnMsg.turtle_high='HIGH';
EnMsg.turtle_low='LOW';

//2wd桌面小车
EnMsg.Desk_on_off='electrical level';

EnMsg.Desk_front='front';
EnMsg.Desk_back='back';
EnMsg.Desk_left='left';
EnMsg.Desk_turn_left='turn_left';
EnMsg.Desk_right='right';
EnMsg.Desk_turn_right='turn_right';
EnMsg.Desk_stop='stop';
EnMsg.Desk_speed='speed';

EnMsg.Desk_L_ir_a='left_infrared_avoid';
EnMsg.Desk_R_ir_a='right_infrared_avoid';
EnMsg.Desk_L_track='left_tracking';
EnMsg.Desk_C_track='center_tracking';
EnMsg.Desk_R_track='right_tracking';

EnMsg.Desk_sr01='ultrasonic';
EnMsg.Desk_buzzer='buzzer';
EnMsg.Desk_notone = 'No_Tone';
EnMsg.Desk_play_music = 'play_music';
EnMsg.Desk_Ode_to_joy = 'Ode_to_Joy';
EnMsg.Desk_birthday = 'birthday';
EnMsg.Desk_fre='frequency';
EnMsg.Desk_ir_R='infrared_module';
EnMsg.Desk_ir_RD='infrared_receive';
EnMsg.Desk_Bluetooth='BLE_module';
EnMsg.Desk_Bluetooth_rec='BLE_receive';

//4wd小车
EnMsg.Mecanumcar='Mecanum car';
EnMsg.Car_state='Car state';
EnMsg.ks4wd_front='front';
EnMsg.ks4wd_back='back';
EnMsg.ks4wd_left='left';
EnMsg.ks4wd_turn_left='turn_left';
EnMsg.ks4wd_right='right';
EnMsg.ks4wd_turn_right='turn_right';
EnMsg.ks4wd_stop='stop';
EnMsg.ks4wd_speed='speed';

EnMsg.ks4wd_choose='choose';

EnMsg.ks4wd_L_ir_a='left_infrared_avoid';
EnMsg.ks4wd_R_ir_a='right_infrared_avoid';
EnMsg.ks4wd_L_track='left_tracking';
EnMsg.ks4wd_C_track='center_tracking';
EnMsg.ks4wd_R_track='right_tracking';

EnMsg.ks4wd_sr01='ultrasonic';
EnMsg.ks4wd_buzzer='buzzer';
EnMsg.ks4wd_notone = 'No_Tone';
EnMsg.ks4wd_play_music = 'play_music';
EnMsg.ks4wd_Ode_to_joy = 'Ode_to_Joy';
EnMsg.ks4wd_birthday = 'birthday';
EnMsg.ks4wd_fre='frequency';

EnMsg.MIXLY_ks4wd_SERVO = 'servo';
EnMsg.MIXLY_ks4wd_matrix = 'Matrix_init';

EnMsg.ks4wd_ir_R='infrared_module';
EnMsg.ks4wd_ir_RD='infrared_receive';
EnMsg.ks4wd_Bluetooth='BT_module';
EnMsg.ks4wd_Bluetooth_rec='BT_receive';
EnMsg.MIXLY_SERIAL_READSTR_UNTIL = 'BT_readStringUntil';

EnMsg.ks4wd_on_off='electrical level';
EnMsg.ks4wd_high='HIGH';
EnMsg.ks4wd_low='LOW';

EnMsg.ksCar_forward='Move forward';
EnMsg.ksCar_backward='Move backward';
EnMsg.ksCar_left='Left rotation';
EnMsg.ksCar_right='Right rotation';

EnMsg.MIXLY_brightness='brightness';
EnMsg.MIXLY_init_brightness='Init brightness';
EnMsg.MIXLY_TIME_FOR_CHANGE_COLOUR='time for change colour';
EnMsg.MIXLY_TIME_FOR_LOOP_COLOUR='time for colour loop';
EnMsg.MIXLY_drawLine='Dot Matrix drawLine';
EnMsg.MIXLY_drawdrawCircle='Dot Matrix drawCircle';
EnMsg.MIXLY_drawRect='Dot Matrix drawRect';
EnMsg.MIXLY_drawRoundRect='Dot Matrix drawRoundRect';
EnMsg.MIXLY_drawTriangle='Dot Matrix drawTriangle';
EnMsg.MIXLY_Scroll_direction='Dot Matrix text scroll direction';
EnMsg.MIXLY_Preset_photo='Dot Matrix preset photo';





//otto_frog
EnMsg.otto_init='Frog_init';
EnMsg.otto_YL='Adjust the left leg';
EnMsg.otto_YR='Adjust the right leg';
EnMsg.otto_RL='Adjust the left foot';
EnMsg.otto_RR='Adjust the right foot';
EnMsg.otto_steps='step number';
EnMsg.otto_speed='speed';
EnMsg.otto_left_moonwalk='left_moonwalk';
EnMsg.otto_right_moonwalk='right_moonwalk';
EnMsg.otto_left_galop='left_galop';
EnMsg.otto_right_galop='right_galop';
EnMsg.otto_drunk='drunk';
EnMsg.otto_noGravity='noGravity';
EnMsg.otto_crusaito='crusaito';
EnMsg.otto_friction_pace='friction_pace';
EnMsg.otto_jump='jump jump jump';
EnMsg.otto_flapping='flapping';
EnMsg.otto_swing='swing';
EnMsg.otto_goingUp='goingUp';

EnMsg.otto_front='front';
EnMsg.otto_back='back';
EnMsg.otto_left='left';
EnMsg.otto_turn_left='turn_left';
EnMsg.otto_right='right';
EnMsg.otto_turn_right='turn_right';
EnMsg.otto_stop='Keep standing';
EnMsg.otto_speed='speed';

EnMsg.otto_L_ir_a='left_infrared_avoid';
EnMsg.otto_R_ir_a='right_infrared_avoid';
EnMsg.otto_L_track='left_tracking';
EnMsg.otto_C_track='center_tracking';
EnMsg.otto_R_track='right_tracking';

EnMsg.otto_servo='Servo';
EnMsg.otto_angle='angle';
EnMsg.otto_sr01='ultrasonic';
EnMsg.otto_buzzer='buzzer';
EnMsg.otto_notone = 'No_Tone';
EnMsg.otto_play_music = 'play_music';
EnMsg.otto_Ode_to_joy = 'Ode_to_Joy';
EnMsg.otto_birthday = 'birthday';
EnMsg.otto_fre='frequency';
EnMsg.otto_ir_R='infrared_module';
EnMsg.otto_ir_RD='infrared_receive';
EnMsg.otto_Bluetooth='BLE_module';
EnMsg.otto_Bluetooth_rec='BLE_receive';






//keyes brick
EnMsg.ke_red_led = 'red_led';
EnMsg.ke_dual_led = 'dual_led';
EnMsg.ke_yellow_led='yellow_led';
EnMsg.ke_white_led='white_led';
EnMsg.ke_3W='3W_led';
EnMsg.ke_led_green='green_led';

EnMsg.ke_msgb_led1='The magic light cup_LED';
EnMsg.ke_msgb_sor1='The magic light cup sensor';
EnMsg.ke_shouzhi1='Finger heart rate';
EnMsg.ke_jg1='laser';

EnMsg.Kids_anologWrite = 'anologWrite';
EnMsg.Kids_value = 'value';


EnMsg.Kids_ON = 'HIGH';
EnMsg.Kids_OFF = 'LOW';
EnMsg.Kids_anologWrite = 'analogWrite';

EnMsg.Kids_iic = 'PIN：SDA# A4, SCL# A5';
EnMsg.Kids_rot = 'button_PIN';
EnMsg.Kids_rot_count = 'count';
EnMsg.Kids_bits = 'string';
EnMsg.Kids_pin = 'PIN';

EnMsg.Kids_iic_pin = 'PIN #SDA:A4,#SCL:A5';
EnMsg.Kids_lcd_p = 'LCD';
EnMsg.Kids_shilihua = 'Instantiation name';
EnMsg.Kids_size = 'font size';

EnMsg.Kids_printcount = 'Display digits';
EnMsg.ke_string = 'display character';

EnMsg.Kids_lcd_left = 'LCD_Scroll to the left';
EnMsg.Kids_lcd_right = 'LCD_Scroll to the right';

EnMsg.ke_TM1637='4 digit 8-segment LED digital';
EnMsg.ke_ws='digit';
EnMsg.ke_begin='Display position';
EnMsg.ke_fill0='add 0?';
EnMsg.ke_light='Brightness0~7';
EnMsg.ke_XY='Show or hide';
EnMsg.ke_L='left';
EnMsg.ke_R='right';
EnMsg.ke_MH='colon';
EnMsg.ke_value='value';

EnMsg.ke_oled_init = 'OLED_init';
EnMsg.ke_oled_piexl = 'OLED_point coordinates';
EnMsg.ke_oled_x = 'column';
EnMsg.ke_oled_y = 'row';
EnMsg.ke_oled_cong = 'from';
EnMsg.ke_oled_dao = 'to';
EnMsg.ke_oled_kai = 'initial point';
EnMsg.ke_oled_kuan = 'width';
EnMsg.ke_oled_chang = 'height';
EnMsg.ke_oled_angle1 = 'angle1';
EnMsg.ke_oled_angle2 = 'angle2';
EnMsg.ke_oled_angle3 = 'angle3';

EnMsg.ke_oled_line = 'OLED_line';
EnMsg.ke_oled_rect = 'OLED_hollow rectangle';
EnMsg.ke_oled_fil_lrect = 'OLED_solid rectangle';
EnMsg.ke_oled_r_rect = 'OLED_hollow rounded rectangle';
EnMsg.ke_oled_r_fill_rect = 'OLED_solid rounded rectangle';
EnMsg.ke_oled_circle = 'OLED_hollow circle  Center coordinates';
EnMsg.ke_oled_circle_radius = 'Circle radius';
EnMsg.ke_oled_radius = 'Corner radius';
EnMsg.ke_oled_fill_circle = 'OLED_solid circle  Center coordinates';
EnMsg.ke_oled_triangle = 'OLED_hollow triangle';
EnMsg.ke_oled_fill_triangle = 'OLED_solid triangle';
EnMsg.ke_oled_string1 = 'OLED_displays a string or number';
EnMsg.ke_oled_weizhi = 'display position';
EnMsg.ke_oled_print = 'display';
EnMsg.ke_oled_clear = 'OLED_clear';


EnMsg.MIXLY_ke_LED1='Piranha LED';
EnMsg.MIXLY_ke_LED2='Red Piranha LED';
EnMsg.MIXLY_ke_LED3='Green Piranha LED';
EnMsg.MIXLY_ke_LED4='Yellow Piranha LED';
EnMsg.MIXLY_ke_LED5='Blue Piranha LED';
EnMsg.MIXLY_ke_LED01='Straw cap LED';
EnMsg.MIXLY_ke_LED02='Red Straw cap LED';
EnMsg.MIXLY_ke_LED03='Green Straw cap LED';
EnMsg.MIXLY_ke_LED04='Yellow straw cap LED';
EnMsg.MIXLY_ke_LED05='Blue Straw cap LED';
EnMsg.MIXLY_ke_QCD='Colorful lights';
EnMsg.MIXLY_ke_RGB='RGB';

EnMsg.MIXLY_ke_BUZZER1='Active buzzer';
EnMsg.MIXLY_ke_BUZZER2='Passive Buzzer';
EnMsg.MIXLY_ke_RELAY='Relay';
EnMsg.MIXLY_ke_MOTOR='Fan';
EnMsg.MIXLY_ke_MOTOR01='geared motor';
EnMsg.MIXLY_ke_SERVO='servo';
EnMsg.MIXLY_ke_TB6612='TB6612motor';
EnMsg.MIXLY_H='front';
EnMsg.MIXLY_L='back';

EnMsg.MIXLY_ke_2812RGB='Full Color Led';

EnMsg.MIXLY_ke_IR_G='PIR Sensor';
EnMsg.MIXLY_ke_FLAME='Flame Sensor';
EnMsg.MIXLY_ke_HALL='Hall Sensor';
EnMsg.MIXLY_ke_CRASH='Crash Sensor';
EnMsg.MIXLY_ke_BUTTON='Button';
EnMsg.MIXLY_ke_TUOCH='Capacitive Touch';
EnMsg.MIXLY_ke_KNOCK='Knock Sensor';
EnMsg.MIXLY_ke_TILT='Tilt Sensor';
EnMsg.MIXLY_ke_SHAKE='Vibration Sensor';
EnMsg.MIXLY_ke_REED_S='Reed Switch Sensor';
EnMsg.MIXLY_ke_TRACK='Tracking Sensor';
EnMsg.MIXLY_ke_AVOID='Obstacle Avoidance MSensor';
EnMsg.MIXLY_ke_LIGHT_B='Light Interrupt Sensor';
EnMsg.MIXLY_ke_ROT='Rotation';


EnMsg.MIXLY_ke_ANALOG_T='Analog Temperature Sensor';
EnMsg.MIXLY_ke_SOUND='Sound Sensor';
EnMsg.MIXLY_ke_LIGHT='photosensitive Sensor';
EnMsg.MIXLY_ke_WATER='Water Level Sensor';
EnMsg.MIXLY_ke_SOIL='Soil Sensor';
EnMsg.MIXLY_ke_POTENTIOMETER='rotational potentiometer';
EnMsg.MIXLY_ke_LM35='LM35 Temperature Sensor';
EnMsg.MIXLY_ke_SLIDE_POTENTIOMETER='slide potentiometer';
EnMsg.MIXLY_ke_TEMT6000='TEMT6000 Ambient Light';
EnMsg.MIXLY_ke_STEAM='water vapor sensor';
EnMsg.MIXLY_ke_FILM_P='Thin-film Pressure Sensor';
EnMsg.MIXLY_ke_JOYSTICK='Joystick Sensor';
EnMsg.MIXLY_ke_JOYSTICK_btn='Joystick_button';
EnMsg.MIXLY_ke_SMOKE='Smoke Sensor';
EnMsg.MIXLY_ke_ALCOHOL='Alcohol Sensor';
EnMsg.MIXLY_ke_MQ135='MQ135 Air Quality';
EnMsg.MIXLY_ke_18B20='18B20 Temperature Sensor';
EnMsg.MIXLY_ke_18B20_R='Getting temperature';
EnMsg.MIXLY_ke_DHT11='temperature and humidity Sensor';
EnMsg.MIXLY_DHT11_H='getTemperature';    /////////////
EnMsg.MIXLY_DHT11_T='getHumidity';     ////////////
EnMsg.MIXLY_ke_BMP180='BMP180 altimeter Sensor';
EnMsg.MIXLY_ke_BMP180_T='temperature';
EnMsg.MIXLY_ke_BMP180_A='atmosphere';
EnMsg.MIXLY_ke_BMP180_H='height above sea level ';

EnMsg.MIXLY_ke_BMP280='BMP280 altimeter Sensor';
EnMsg.MIXLY_ke_BMP280_T='temperature';
EnMsg.MIXLY_ke_BMP280_A='atmosphere';
EnMsg.MIXLY_ke_BMP280_H='height above sea level';

EnMsg.MIXLY_ke_SR01='SR01 Ultrasound Module';
EnMsg.MIXLY_ke_3231='DS3231 clock';
EnMsg.MIXLY_ke_ADXL345='Acceleration Sensor';
EnMsg.MIXLY_ADXL345_X='X-axis acceleration'; ///
EnMsg.MIXLY_ADXL345_Y='Y-axis acceleration'; ///
EnMsg.MIXLY_ADXL345_Z='Z-axis acceleration'; ///
EnMsg.MIXLY_ADXL345_XA='X-axis angle';  ///
EnMsg.MIXLY_ADXL345_YA='Y-axis angle';  ///


EnMsg.MIXLY_ke_OLED='OLED_displays a string or number';
EnMsg.MIXLY_ke_1602LCD='IIC1602LCD';
EnMsg.MIXLY_SETUP='setup';  ////////////////
EnMsg.MIXLY_LCD_ADDRESS='address'; /////////
EnMsg.MIXLY_LCD_PRINT1='print line1';  /////////
EnMsg.MIXLY_LCD_PRINT2='print line2'; ///////////
EnMsg.MIXLY_LCD_ROW='row'; /////
EnMsg.MIXLY_LCD_COLUMN='column'; /////////
EnMsg.MIXLY_LCD_PRINT='print'; ////
EnMsg.MIXLY_LCD_STAT_ON='On';  /////
EnMsg.MIXLY_LCD_STAT_OFF='Off';  /////
EnMsg.MIXLY_LCD_STAT_CURSOR='Cursor';  ////
EnMsg.MIXLY_LCD_STAT_NOCURSOR='noCursor';  ////
EnMsg.MIXLY_LCD_STAT_BLINK='Blink';  ////
EnMsg.MIXLY_LCD_STAT_NOBLINK='noBlink'; ///
EnMsg.MIXLY_LCD_STAT_CLEAR='Clear';  ///
EnMsg.MIXLY_LCD_NOBACKLIGHT='NoBackLight'; ///
EnMsg.MIXLY_LCD_BACKLIGHT='BackLight'; ///
EnMsg.MIXLY_ke_2004LCD='IIC2004LCD';
EnMsg.MIXLY_ke_print1='print line1';
EnMsg.MIXLY_ke_print2='print line2';
EnMsg.MIXLY_ke_print3='print line3';
EnMsg.MIXLY_ke_print4='print line4';

EnMsg.MIXLY_ke_MATRIX='8*8 dot matrix';
EnMsg.MIXLY_ke_TM1637='4 digit 8-segment LED digital';
EnMsg.MIXLY_ke_TM1637_C='digit';
EnMsg.MIXLY_ke_TM1637_P='display position';
EnMsg.MIXLY_ke_TM1637_Fill='add 0?';
EnMsg.MIXLY_ke_TM1637_light='brightness 0~7';
EnMsg.MIXLY_ke_TM1637_xy='show or hide';
EnMsg.MIXLY_ke_TM1637_left='left';
EnMsg.MIXLY_ke_TM1637_maohao='colon';
EnMsg.MIXLY_ke_TM1637_right='right';
EnMsg.MIXLY_ke_value='value';


EnMsg.MIXLY_ke_IR_E='Infrared Transmitter Module';
EnMsg.MIXLY_ke_IR_R='Infrared Receiver Module';
EnMsg.MIXLY_ke_W5100='W5100 Ethernet Module';
EnMsg.MIXLY_ke_BLUETOOTH='Bluetooth 2.0 Module';
EnMsg.MIXLY_ke_read='Received signal';


//EnMsg.MIXLY_ke_kzsc = 'Control output';

EnMsg.MIXLY_ke_Count='count';

EnMsg.MIXLY_ke_YEAR = 'year';
EnMsg.MIXLY_ke_MONTH = 'month';
EnMsg.MIXLY_ke_DAY = 'day';
EnMsg.MIXLY_ke_HOUR = 'hour';
EnMsg.MIXLY_ke_MINUTE = 'minute';
EnMsg.MIXLY_ke_SECOND = 'second';
EnMsg.MIXLY_ke_WEEK = 'week';

EnMsg.MIXLY_ke_angle = 'angle';

EnMsg.kids_Ode_to_joy = "Ode_to_joy";
EnMsg.kids_birthday = "birthday";

EnMsg.kids_tone = "tone";
EnMsg.kids_beat = "beat";
EnMsg.kids_play_tone = "play_tone";
EnMsg.kids_notone = "turn off the buzzer";

EnMsg.kids_ADkey = "7 key module";


//////////////keyestudio/////////////////////
EnMsg.Ks_ON='HIGH';
EnMsg.Ks_OFF='LOW';

EnMsg.ks_MQ_d='Flammable gas Sensor-digital';
EnMsg.ks_MQ_a='Flammable gas Sensor-analog';

EnMsg.KS_LED3wd='LED 3W';
EnMsg.KS_LED_W='white_LED';
EnMsg.KS_LED_R='Red_LED';
EnMsg.KS_LED_G='Green_LED';
EnMsg.KS_LED_B='Blue_LED';
EnMsg.KS_LED_Y='Yellow_LED';

EnMsg.MIXLY_KS_BUZZER1='Active buzzer';
EnMsg.MIXLY_KS_BUZZER2='Passive Buzzer';
EnMsg.ks_tone='tone';
EnMsg.ks_beat='beat';
EnMsg.ks_music='play_tone';
EnMsg.ks_Ode_to_joy='Ode_to_joy';
EnMsg.ks_birthday='birthday';
EnMsg.ks_tetris='tetris';
EnMsg.ks_star_war='star_war';
EnMsg.ks_super_mario='super_mario';
EnMsg.ks_christmas='christmas';
EnMsg.ks_notone1='turn off the buzzer';


EnMsg.MIXLY_KS_RELAY='Relay';
EnMsg.MIXLY_KS_MOTOR='Motor';
EnMsg.MIXLY_KS_SERVO='servo';
EnMsg.MIXLY_KS_2812RGB='2812RGB ';

EnMsg.MIXLY_KS_IR_G='PIR Sensor';
EnMsg.MIXLY_KS_FLAME='Flame_Sensor_D';
EnMsg.MIXLY_KS_FLAM_a='Flame_Sensor_a';
EnMsg.MIXLY_KS_HALL='Hall Sensor';
EnMsg.MIXLY_KS_CRASH='Crash Sensor';
EnMsg.MIXLY_KS_BUTTON='Button';
EnMsg.MIXLY_KS_TUOCH='Capacitive Touch';
EnMsg.MIXLY_KS_KNOCK='Knock Module';
EnMsg.MIXLY_KS_TILT='Tilt Module';
EnMsg.MIXLY_KS_SHAKE='Vibration Module';
EnMsg.MIXLY_KS_REED_S='Reed Switch Module';
EnMsg.MIXLY_KS_TRACK='Tracking Module';
EnMsg.MIXLY_KS_AVOID='Obstacle Avoidance Module';
EnMsg.MIXLY_KS_LIGHT_B='Photo Interrupt Module';

EnMsg.MIXLY_KS_ANALOG_T='Analog Temperature Sensor';
EnMsg.MIXLY_KS_SOUND='Sound Sensor';
EnMsg.KS_LIGHT='Photocell Sensor';
EnMsg.MIXLY_KS_WATER='Water Level Sensor';
EnMsg.KS_SOIL='Soil moisture Sensor';
EnMsg.MIXLY_KS_POTENTIOMETER='rotate potentiometer';
EnMsg.MIXLY_KS_LM35='LM35 Temperature Sensor';
EnMsg.MIXLY_KS_SLIDE_POTENTIOMETER='slide potentiometer';
EnMsg.MIXLY_KS_TEMT6000='TEMT6000 Ambient Light';
EnMsg.KS_STEAM='water sensor';
EnMsg.MIXLY_KS_FILM_P='Thin-film Pressure Sensor';
EnMsg.MIXLY_KS_JOYSTICK='Joystick Module';
EnMsg.MIXLY_KS_SMOKE='Smoke Sensor';
EnMsg.MIXLY_KS_ALCOHOL='Alcohol Sensor';
EnMsg.MIXLY_KS_MQ135='MQ135 Air Quality';
EnMsg.MIXLY_KS_18B20='18B20 Temperature Module';
EnMsg.MIXLY_KS_RT='temperature';

EnMsg.MIXLY_KS_DHT11='temperature and humidity module';
EnMsg.MIXLY_DHT11_H='getTemperature';    /////////////
EnMsg.MIXLY_DHT11_T='getHumidity';     ////////////
EnMsg.MIXLY_KS_BMP180='BMP180 altimeter module';
EnMsg.MIXLY_KS_T='temperature';
EnMsg.MIXLY_KS_QY='barometric pressure';
EnMsg.MIXLY_KS_H='altitude';

EnMsg.KS_SR01='Ultrasonic Module';
EnMsg.MIXLY_KS_3231='DS3231 clock';
EnMsg.MIXLY_KS_GET = 'get DS3231 time';
EnMsg.KS_ADXL345='Acceleration Sensor ADXL345';
EnMsg.MIXLY_ADXL345_X='X-axis acceleration'; ///
EnMsg.MIXLY_ADXL345_Y='Y-axis acceleration'; ///
EnMsg.MIXLY_ADXL345_Z='Z-axis acceleration'; ///
EnMsg.MIXLY_ADXL345_XA='X-axis angle';  ///
EnMsg.MIXLY_ADXL345_YA='Y-axis angle';  ///



EnMsg.MIXLY_KS_OLED='OLED Module';
EnMsg.MIXLY_SETUP='setup';  ////////////////
EnMsg.MIXLY_LCD_ADDRESS='address'; /////////
EnMsg.MIXLY_LCD_PRINT1='peint line1';  /////////
EnMsg.MIXLY_LCD_PRINT2='print line2'; ///////////
EnMsg.MIXLY_LCD_ROW='row'; /////
EnMsg.MIXLY_LCD_COLUMN='column'; /////////
EnMsg.MIXLY_LCD_PRINT='print'; ////
EnMsg.MIXLY_LCD_STAT_ON='On';  /////
EnMsg.MIXLY_LCD_STAT_OFF='Off';  /////
EnMsg.MIXLY_LCD_STAT_CURSOR='Cursor';  ////
EnMsg.MIXLY_LCD_STAT_NOCURSOR='noCursor';  ////
EnMsg.MIXLY_LCD_STAT_BLINK='Blink';  ////
EnMsg.MIXLY_LCD_STAT_NOBLINK='noBlink'; ///
EnMsg.MIXLY_LCD_STAT_CLEAR='Clear';  ///
EnMsg.MIXLY_LCD_NOBACKLIGHT='NoBackLight'; ///
EnMsg.MIXLY_LCD_BACKLIGHT='BackLight'; ///
EnMsg.MIXLY_KS_1602LCD='IIC1602LCD';
EnMsg.MIXLY_KS_2004LCD='IIC2004LCD';
EnMsg.MIXLY_KS_MATRIX='8*8 dot matrix';
EnMsg.MIXLY_KS_TM1637='4 digit 8-segment LED digital';
EnMsg.MIXLY_KS_ws='digit';
EnMsg.MIXLY_KS_begin='Display position';
EnMsg.MIXLY_KS_fill0='add 0?';
EnMsg.MIXLY_KS_light='Brightness0~7';
EnMsg.MIXLY_KS_XY='Show or hide';
EnMsg.MIXLY_KS_L='left';
EnMsg.MIXLY_KS_R='right';
EnMsg.MIXLY_KS_MH='colon';
EnMsg.MIXLY_KS_one='print line1';
EnMsg.MIXLY_KS_two='print line2';
EnMsg.MIXLY_KS_three='print line3';
EnMsg.MIXLY_KS_four='print line4';
EnMsg.MIXLY_KS_clear='             clear:';


EnMsg.MIXLY_KS_value='value';


EnMsg.MIXLY_KS_IR_E='Infrared Transmitter Module';
EnMsg.MIXLY_KS_IR_R='Infrared Receiver Module';
EnMsg.MIXLY_KS_W5100='W5100 Ethernet Module';
EnMsg.KS_BLUETOOTH='Bluetooth Module';
EnMsg.MIXLY_KS_rec='Received';


//EnMsg.MIXLY_KS_kzsc = 'Control output';

EnMsg.MIXLY_KS_Count='count';

EnMsg.ks_test_V='voltage sensor';
EnMsg.ks_test_A='current sensor';
EnMsg.MIXLY_KS_FLAME_a='Flame sensor _ analog value';
EnMsg.joys_btn='Rocker button';
EnMsg.hwwd='Non-contact infrared sensor,Pin for SDA : A4 , SCL : A5';
EnMsg.color_sensor='color sensor';
EnMsg.RED_VAL='red value';
EnMsg.GREEN_VAL='green value';
EnMsg.BLUE_VAL='blue value';

EnMsg.matrix16and8_init='Matrix 16*8 init';
EnMsg.matrix16and8='Matrix 16*8 custom';
EnMsg.matrix16and8_image='Matrix 16*8 picture';

EnMsg.Ultraviolet='Ultraviolet Sensor';
EnMsg.WaterTurbidity='WaterTurbidity';
EnMsg.C2H5O='harmful gas Sensor';
EnMsg.CeramicVibration='Piezo Vibration';
EnMsg.DustSensor='Dust Sensor';

EnMsg.MMA8452 = 'Accelerometer_MMA8452';
EnMsg.SHT31 = 'temperature and humidity sensor SHT31';

EnMsg.ks_neopixel = 'RGB neopixel';
EnMsg.ks_neopixel_show = 'neopixel show';

EnMsg.ks_TM1637_INIT ='Four digit tube TM1637 init';
EnMsg.ks_4DIGITDISPLAY = 'Four digit tube TM1637 display';
EnMsg.ks_4DIGITDISPLAY_time = 'Four digit tube TM1637 display time';
EnMsg.ks_4DIGITDISPLAY_brightness = 'Four digit tube TM1637 brightness';
EnMsg.ks_4DIGITDISPLAY_clean = 'Four digit tube TM1637 clean';


EnMsg.ks_SPI_recv = 'Receive data Get register data';
EnMsg.ks_SPI_run = 'perform';
EnMsg.ks_SPI_read = 'The SPI retrieves register data from the machine';

export const EnCatgories = {};