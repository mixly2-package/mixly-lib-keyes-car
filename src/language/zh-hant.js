export const ZhHantMsg = {};

//科易互动机器人-坦克小车
ZhHantMsg.tank_front='前进';
ZhHantMsg.tank_back='后退';
ZhHantMsg.tank_left='左边';
ZhHantMsg.tank_turn_left='左转弯';
ZhHantMsg.tank_right='右边';
ZhHantMsg.tank_turn_right='右转弯';
ZhHantMsg.tank_stop='停止';
ZhHantMsg.tank_speed='速度值';
ZhHantMsg.left_light_sensor='左光线传感器';
ZhHantMsg.right_light_sensor='右光线传感器';
ZhHantMsg.tank_choose='选择';

ZhHantMsg.tank_L_ir_a='左红外避障';
ZhHantMsg.tank_R_ir_a='右红外避障';
ZhHantMsg.tank_L_track='左循迹传感器';
ZhHantMsg.tank_C_track='中循迹传感器';
ZhHantMsg.tank_R_track='右循迹传感器';

ZhHantMsg.tank_sr01='超声波';
ZhHantMsg.tank_buzzer='蜂鸣器';
ZhHantMsg.tank_notone = '关闭蜂鸣器';
ZhHantMsg.tank_play_music = '播放乐曲';
ZhHantMsg.tank_Ode_to_joy = '欢乐颂';
ZhHantMsg.tank_birthday = '生日快乐';
ZhHantMsg.tank_fre='频率';

ZhHantMsg.MIXLY_Tank_SERVO = '舵机';
ZhHantMsg.MIXLY_Tank_matrix = '点阵初始化';
ZhHantMsg.tank_ir_R='红外接收模块';
ZhHantMsg.tank_ir_RD='红外接收数据';
ZhHantMsg.tank_Bluetooth='蓝牙BLE模块';
ZhHantMsg.tank_Bluetooth_rec='蓝牙接收数据';

ZhHantMsg.tank_on_off='电平为';
ZhHantMsg.tank_high='高';
ZhHantMsg.tank_low='低';

//小乌龟车
ZhHantMsg.turtle_front='前进';
ZhHantMsg.turtle_back='后退';
ZhHantMsg.turtle_left='左边';
ZhHantMsg.turtle_turn_left='左转弯';
ZhHantMsg.turtle_right='右边';
ZhHantMsg.turtle_turn_right='右转弯';
ZhHantMsg.turtle_stop='停止';
ZhHantMsg.turtle_speed='速度值';

ZhHantMsg.turtle_L_ir_a='左红外避障';
ZhHantMsg.turtle_R_ir_a='右红外避障';
ZhHantMsg.turtle_L_track='左循迹传感器';
ZhHantMsg.turtle_C_track='中循迹传感器';
ZhHantMsg.turtle_R_track='右循迹传感器';

ZhHantMsg.turtle_sr01='超声波';
ZhHantMsg.turtle_buzzer='蜂鸣器';
ZhHantMsg.turtle_notone = '关闭蜂鸣器';
ZhHantMsg.turtle_play_music = '播放乐曲';
ZhHantMsg.turtle_Ode_to_joy = '欢乐颂';
ZhHantMsg.turtle_birthday = '生日快乐';
ZhHantMsg.turtle_fre='频率';

ZhHantMsg.MIXLY_turtle_SERVO = '舵机';
ZhHantMsg.MIXLY_turtle_matrix = '点阵初始化';

ZhHantMsg.turtle_ir_R='红外接收模块';
ZhHantMsg.turtle_ir_RD='红外接收数据';
ZhHantMsg.turtle_Bluetooth='蓝牙BLE模块';
ZhHantMsg.turtle_Bluetooth_rec='蓝牙接收数据';

ZhHantMsg.turtle_on_off='电平为';
ZhHantMsg.turtle_high='高';
ZhHantMsg.turtle_low='低';

//2wd桌面小车
ZhHantMsg.Desk_on_off='电平为';

ZhHantMsg.Desk_front='前进';
ZhHantMsg.Desk_back='后退';
ZhHantMsg.Desk_left='左旋转';
ZhHantMsg.Desk_turn_left='左转弯';
ZhHantMsg.Desk_right='右旋转';
ZhHantMsg.Desk_turn_right='右转弯';
ZhHantMsg.Desk_stop='停止';
ZhHantMsg.Desk_speed='速度值';

ZhHantMsg.Desk_L_ir_a='左红外避障';
ZhHantMsg.Desk_R_ir_a='右红外避障';
ZhHantMsg.Desk_L_track='左循迹传感器';
ZhHantMsg.Desk_C_track='中循迹传感器';
ZhHantMsg.Desk_R_track='右循迹传感器';

ZhHantMsg.Desk_sr01='超声波';
ZhHantMsg.Desk_buzzer='蜂鸣器';
ZhHantMsg.Desk_notone = '关闭蜂鸣器';
ZhHantMsg.Desk_play_music = '播放乐曲';
ZhHantMsg.Desk_Ode_to_joy = '欢乐颂';
ZhHantMsg.Desk_birthday = '生日快乐';
ZhHantMsg.Desk_fre='频率';
ZhHantMsg.Desk_ir_R='红外接收模块';
ZhHantMsg.Desk_ir_RD='红外接收数据';
ZhHantMsg.Desk_Bluetooth='蓝牙BLE模块';
ZhHantMsg.Desk_Bluetooth_rec='蓝牙接收数据';

//4wd小车
ZhHantMsg.Mecanumcar='麦轮小车';
ZhHantMsg.Car_state='小車狀態為';
ZhHantMsg.ks4wd_front='前进';
ZhHantMsg.ks4wd_back='后退';
ZhHantMsg.ks4wd_left='左边';
ZhHantMsg.ks4wd_turn_left='左转弯';
ZhHantMsg.ks4wd_right='右边';
ZhHantMsg.ks4wd_turn_right='右转弯';
ZhHantMsg.ks4wd_stop='停止';
ZhHantMsg.ks4wd_speed='速度值';

ZhHantMsg.ks4wd_choose='选择';

ZhHantMsg.ks4wd_L_ir_a='左红外避障';
ZhHantMsg.ks4wd_R_ir_a='右红外避障';
ZhHantMsg.ks4wd_L_track='左循迹传感器';
ZhHantMsg.ks4wd_C_track='中循迹传感器';
ZhHantMsg.ks4wd_R_track='右循迹传感器';

ZhHantMsg.ks4wd_sr01='超声波';
ZhHantMsg.ks4wd_buzzer='蜂鸣器';
ZhHantMsg.ks4wd_notone = '关闭蜂鸣器';
ZhHantMsg.ks4wd_play_music = '播放乐曲';
ZhHantMsg.ks4wd_Ode_to_joy = '欢乐颂';
ZhHantMsg.ks4wd_birthday = '生日快乐';
ZhHantMsg.ks4wd_fre='频率';

ZhHantMsg.MIXLY_ks4wd_SERVO = '舵机';
ZhHantMsg.MIXLY_ks4wd_matrix = '点阵初始化';

ZhHantMsg.ks4wd_ir_R='红外接收模块';
ZhHantMsg.ks4wd_ir_RD='红外接收数据';
ZhHantMsg.ks4wd_Bluetooth='蓝牙模块';
ZhHantMsg.ks4wd_Bluetooth_rec='蓝牙接收数据';
ZhHantMsg.MIXLY_SERIAL_READSTR_UNTIL = '蓝牙讀取字串直到';

ZhHantMsg.ks4wd_on_off='电平为';
ZhHantMsg.ks4wd_high='高';
ZhHantMsg.ks4wd_low='低';

ZhHantMsg.ksCar_forward='前进';
ZhHantMsg.ksCar_backward='后退';
ZhHantMsg.ksCar_left='左旋转';
ZhHantMsg.ksCar_right='右旋转';

ZhHantMsg.MIXLY_brightness='亮度';
ZhHantMsg.MIXLY_init_brightness='初始亮度';
ZhHantMsg.MIXLY_TIME_FOR_CHANGE_COLOUR='七彩变换切换时间';
ZhHantMsg.MIXLY_TIME_FOR_LOOP_COLOUR='七彩循环切换时间';
ZhHantMsg.MIXLY_drawLine='点阵画直线 ';
ZhHantMsg.MIXLY_drawdrawCircle='点阵画圆';
ZhHantMsg.MIXLY_drawRect='点阵画矩形';
ZhHantMsg.MIXLY_drawRoundRect='点阵画圆角矩形';
ZhHantMsg.MIXLY_drawTriangle='点阵画三角形';
ZhHantMsg.MIXLY_Scroll_direction='点阵屏滚动文字方向';
ZhHantMsg.MIXLY_Preset_photo='点阵预设图案';



//otto_FROG
ZhHantMsg.otto_init='初始化';
ZhHantMsg.otto_YL='调整左腿';
ZhHantMsg.otto_YR='调整右腿';
ZhHantMsg.otto_RL='调整左脚';
ZhHantMsg.otto_RR='调整右脚';
ZhHantMsg.otto_steps='步数';
ZhHantMsg.otto_speed='速度';
ZhHantMsg.otto_left_moonwalk='左太空步';
ZhHantMsg.otto_right_moonwalk='右太空步';
ZhHantMsg.otto_left_galop='左快步';
ZhHantMsg.otto_right_galop='右快步';
ZhHantMsg.otto_drunk='摆动';
ZhHantMsg.otto_noGravity='失重状态中';
ZhHantMsg.otto_crusaito='滑步';
ZhHantMsg.otto_friction_pace='摩擦摩擦';
ZhHantMsg.otto_jump='跳跳跳。。。';
ZhHantMsg.otto_flapping='摇摆运动';
ZhHantMsg.otto_swing='游泳姿势';
ZhHantMsg.otto_goingUp='慢慢踮起脚';
ZhHantMsg.otto_front='前进';
ZhHantMsg.otto_back='后退';
ZhHantMsg.otto_left='左旋转';
ZhHantMsg.otto_turn_left='左转弯';
ZhHantMsg.otto_right='右旋转';
ZhHantMsg.otto_turn_right='右转弯';
ZhHantMsg.otto_stop='保持站立';
ZhHantMsg.otto_speed='速度值';
ZhHantMsg.otto_servo='舵机';
ZhHantMsg.otto_angle='角度';
ZhHantMsg.otto_sr01='超声波';
ZhHantMsg.otto_Bluetooth='蓝牙BLE模块';
ZhHantMsg.otto_Bluetooth_rec='蓝牙接收数据';


//keyes brick
ZhHantMsg.ke_red_led = '红色led灯';
ZhHantMsg.ke_dual_led = '双色灯';
ZhHantMsg.ke_yellow_led='黄色LED灯';
ZhHantMsg.ke_white_led='白色LED灯';
ZhHantMsg.ke_3W='3W_LED灯';
ZhHantMsg.ke_led_green='绿色灯';


ZhHantMsg.ke_msgb_led1='魔术光杯_LED';
ZhHantMsg.ke_msgb_sor1='魔术光杯传感器';
ZhHantMsg.ke_shouzhi1='手指测心率';
ZhHantMsg.ke_jg1='激光';


ZhHantMsg.MIXLY_ke_QCD='七彩led灯';
ZhHantMsg.MIXLY_ke_RGB='RGB';

ZhHantMsg.Kids_anologWrite = 'PWM模拟输出';
ZhHantMsg.Kids_value = '赋值为';

ZhHantMsg.Kids_iic = '管脚：SDA# A4, SCL# A5';
ZhHantMsg.Kids_rot = '按钮管脚';
ZhHantMsg.Kids_rot_count = '计数变量';
ZhHantMsg.Kids_bits = '字符串';
ZhHantMsg.Kids_pin = '管脚';

ZhHantMsg.Kids_iic_pin = '管脚SDA:A4,SCL:A5';
ZhHantMsg.Kids_lcd_p = '液晶显示屏';
ZhHantMsg.Kids_shilihua = '实例化名称';
ZhHantMsg.Kids_size = '字体大小';
ZhHantMsg.Kids_printcount = '显示数字';

ZhHantMsg.ke_string = '显示字符';

ZhHantMsg.Kids_lcd_left = '液晶显示屏往左滚动';
ZhHantMsg.Kids_lcd_right = '液晶显示屏往右滚动';

ZhHantMsg.ke_TM1637='4位8段数码管';
ZhHantMsg.ke_ws='位数';
ZhHantMsg.ke_begin='显示的位置';
ZhHantMsg.ke_fill0='是否补充0';
ZhHantMsg.ke_light='亮度0~7';
ZhHantMsg.ke_XY='显或隐';
ZhHantMsg.ke_L='左边';
ZhHantMsg.ke_R='右边';
ZhHantMsg.ke_MH='冒号';
ZhHantMsg.ke_value='数值';


ZhHantMsg.ke_oled_init = 'OLED初始化';
ZhHantMsg.ke_oled_piexl = 'OLED_画点的坐标';
ZhHantMsg.ke_oled_x = '列';
ZhHantMsg.ke_oled_y = '行';
ZhHantMsg.ke_oled_cong = '从';
ZhHantMsg.ke_oled_dao = '到';
ZhHantMsg.ke_oled_kai = '起始位';
ZhHantMsg.ke_oled_kuan = '宽';
ZhHantMsg.ke_oled_chang = '长';
ZhHantMsg.ke_oled_angle1 = '角度1为';
ZhHantMsg.ke_oled_angle2 = '角度2为';
ZhHantMsg.ke_oled_angle3 = '角度3为';

ZhHantMsg.ke_oled_line = 'OLED_画两点连线';
ZhHantMsg.ke_oled_rect = 'OLED_画空心矩形';
ZhHantMsg.ke_oled_fil_lrect = 'OLED_画实心矩形';
ZhHantMsg.ke_oled_r_rect = 'OLED_画倒圆角的空心矩形';
ZhHantMsg.ke_oled_r_fill_rect = 'OLED_画倒圆角的实心矩形';
ZhHantMsg.ke_oled_circle = 'OLED_画空心圆形  圆心坐标';
ZhHantMsg.ke_oled_circle_radius = '圆半径';
ZhHantMsg.ke_oled_radius = '圆角半径';
ZhHantMsg.ke_oled_fill_circle = 'OLED_画实心的圆形  圆心坐标';
ZhHantMsg.ke_oled_triangle = 'OLED_画空心三角形';
ZhHantMsg.ke_oled_fill_triangle = 'OLED_画实心三角形';
ZhHantMsg.ke_oled_string1 = 'OLED_显示字符串或数字';
ZhHantMsg.ke_oled_weizhi = '显示的位置';
ZhHantMsg.ke_oled_print = '显示';
ZhHantMsg.ke_oled_clear = 'OLED_清屏';




ZhHantMsg.Kids_ON = '高';
ZhHantMsg.Kids_OFF = '低';
ZhHantMsg.Kids_anologWrite = 'PWM模拟输出';



ZhHantMsg.MIXLY_ke_BUZZER1='有源蜂鸣器';
ZhHantMsg.MIXLY_ke_BUZZER2='无源蜂鸣器';
ZhHantMsg.MIXLY_ke_RELAY='继电器';
ZhHantMsg.MIXLY_ke_MOTOR='小风扇';
ZhHantMsg.MIXLY_ke_MOTOR01='减速电机';
ZhHantMsg.MIXLY_ke_SERVO='舵机';
ZhHantMsg.MIXLY_ke_TB6612='TB6612电机驱动';
ZhHantMsg.MIXLY_H='正';
ZhHantMsg.MIXLY_L='反';

ZhHantMsg.MIXLY_ke_2812RGB='全彩led灯模块';

ZhHantMsg.MIXLY_ke_IR_G='人体红外热传感器';
ZhHantMsg.MIXLY_ke_FLAME='火焰传感器';
ZhHantMsg.MIXLY_ke_HALL='霍尔传感器';
ZhHantMsg.MIXLY_ke_CRASH='碰撞传感器';
ZhHantMsg.MIXLY_ke_BUTTON='按键';
ZhHantMsg.MIXLY_ke_TUOCH='触摸传感器';
ZhHantMsg.MIXLY_ke_KNOCK='敲击传感器';
ZhHantMsg.MIXLY_ke_TILT='倾斜传感器';
ZhHantMsg.MIXLY_ke_SHAKE='震动传感器';
ZhHantMsg.MIXLY_ke_REED_S='干簧管传感器';
ZhHantMsg.MIXLY_ke_TRACK='循迹传感器';
ZhHantMsg.MIXLY_ke_AVOID='避障传感器';
ZhHantMsg.MIXLY_ke_LIGHT_B='光折断传感器';
ZhHantMsg.MIXLY_ke_ROT='旋转编码器';

ZhHantMsg.MIXLY_ke_ANALOG_T='模拟温度传感器';
ZhHantMsg.MIXLY_ke_SOUND='声音传感器';
ZhHantMsg.MIXLY_ke_LIGHT='光敏传感器';
ZhHantMsg.MIXLY_ke_WATER='水位传感器';
ZhHantMsg.MIXLY_ke_SOIL='土壤传感器';
ZhHantMsg.MIXLY_ke_POTENTIOMETER='旋转电位器';
ZhHantMsg.MIXLY_ke_LM35='LM35温度传感器';
ZhHantMsg.MIXLY_ke_SLIDE_POTENTIOMETER='滑动电位器';
ZhHantMsg.MIXLY_ke_TEMT6000='TEMT6000环境光';
ZhHantMsg.MIXLY_ke_STEAM='水蒸气传感器';
ZhHantMsg.MIXLY_ke_FILM_P='薄膜压力传感器';
ZhHantMsg.MIXLY_ke_JOYSTICK='遥杆传感器';
ZhHantMsg.MIXLY_ke_JOYSTICK_btn='遥杆按钮';
ZhHantMsg.MIXLY_ke_SMOKE='烟雾传感器';
ZhHantMsg.MIXLY_ke_ALCOHOL='酒精传感器';
ZhHantMsg.MIXLY_ke_MQ135='MQ135空气质量';
ZhHantMsg.MIXLY_ke_18B20='18B20温度传感器';
ZhHantMsg.MIXLY_ke_18B20_R='获取温度';
ZhHantMsg.MIXLY_ke_DHT11='温湿度传感器';
ZhHantMsg.MIXLY_DHT11_H='获取湿度';    /////////////
ZhHantMsg.MIXLY_DHT11_T='获取温度';     ////////////
ZhHantMsg.MIXLY_ke_BMP180='BMP180高度计传感器';

ZhHantMsg.MIXLY_ke_BMP180_T='温度';
ZhHantMsg.MIXLY_ke_BMP180_A='大气压';
ZhHantMsg.MIXLY_ke_BMP180_H='海拔高度';

ZhHantMsg.MIXLY_ke_BMP280='BMP280高度计传感器';
ZhHantMsg.MIXLY_ke_BMP280_T='温度';
ZhHantMsg.MIXLY_ke_BMP280_A='大气压';
ZhHantMsg.MIXLY_ke_BMP280_H='海拔高度';

ZhHantMsg.MIXLY_ke_SR01='超声波模块';
ZhHantMsg.MIXLY_ke_3231='DS3231时钟';
ZhHantMsg.MIXLY_ke_ADXL345='加速度传感器';
ZhHantMsg.MIXLY_ADXL345_X='X轴加速度'; ///
ZhHantMsg.MIXLY_ADXL345_Y='Y轴加速度'; ///
ZhHantMsg.MIXLY_ADXL345_Z='Z轴加速度'; ///
ZhHantMsg.MIXLY_ADXL345_XA='X轴角度';  ///
ZhHantMsg.MIXLY_ADXL345_YA='Y轴角度';  ///

ZhHantMsg.MIXLY_ke_YEAR = '年';
ZhHantMsg.MIXLY_ke_MONTH = '月';
ZhHantMsg.MIXLY_ke_DAY = '天';
ZhHantMsg.MIXLY_ke_TEXT = '周';
ZhHantMsg.MIXLY_ke_HOUR = '时';
ZhHantMsg.MIXLY_ke_MINUTE = '分';
ZhHantMsg.MIXLY_ke_SECOND = '秒';
ZhHantMsg.MIXLY_ke_GET = '获取时间';


ZhHantMsg.MIXLY_ke_OLED='OLED_显示字符串或数字';
ZhHantMsg.MIXLY_ke_1602LCD='IIC1602LCD';
ZhHantMsg.MIXLY_SETUP='初始化';  ////////////////
ZhHantMsg.MIXLY_LCD_ADDRESS='设备地址'; /////////
ZhHantMsg.MIXLY_LCD_PRINT1='打印第一行';  /////////
ZhHantMsg.MIXLY_LCD_PRINT2='打印第二行'; ///////////
ZhHantMsg.MIXLY_LCD_ROW='在第'; /////
ZhHantMsg.MIXLY_LCD_COLUMN='行第'; /////////
ZhHantMsg.MIXLY_LCD_PRINT='列打印'; ////
ZhHantMsg.MIXLY_LCD_STAT_ON='开';  /////
ZhHantMsg.MIXLY_LCD_STAT_OFF='关';  /////
ZhHantMsg.MIXLY_LCD_STAT_CURSOR='有光标';  ////
ZhHantMsg.MIXLY_LCD_STAT_NOCURSOR='无光标';  ////
ZhHantMsg.MIXLY_LCD_STAT_BLINK='闪烁';  ////
ZhHantMsg.MIXLY_LCD_STAT_NOBLINK='不闪烁'; ///
ZhHantMsg.MIXLY_LCD_STAT_CLEAR='清屏';  ///
ZhHantMsg.MIXLY_LCD_NOBACKLIGHT='关闭背光'; ///
ZhHantMsg.MIXLY_LCD_BACKLIGHT='打开背光'; ///

ZhHantMsg.MIXLY_ke_2004LCD='IIC2004LCD';
ZhHantMsg.MIXLY_ke_print1='打印第一行';
ZhHantMsg.MIXLY_ke_print2='打印第二行';
ZhHantMsg.MIXLY_ke_print3='打印第三行';
ZhHantMsg.MIXLY_ke_print4='打印第四行';


ZhHantMsg.MIXLY_ke_MATRIX='8*8点阵';
ZhHantMsg.MIXLY_ke_TM1637='4位8段数码管';
ZhHantMsg.MIXLY_ke_TM1637_C='位数';
ZhHantMsg.MIXLY_ke_TM1637_P='在第几位开始显示';
ZhHantMsg.MIXLY_ke_TM1637_Fill='是否填充0';
ZhHantMsg.MIXLY_ke_TM1637_light='亮度0~7';
ZhHantMsg.MIXLY_ke_TM1637_xy='显或隐';
ZhHantMsg.MIXLY_ke_TM1637_left='左边';
ZhHantMsg.MIXLY_ke_TM1637_maohao='冒号';
ZhHantMsg.MIXLY_ke_TM1637_right='右边';
ZhHantMsg.MIXLY_ke_value='数值';

ZhHantMsg.MIXLY_ke_IR_E='红外发射模块';
ZhHantMsg.MIXLY_ke_IR_R='红外接收模块';
ZhHantMsg.MIXLY_ke_W5100='W5100以太网模块';
ZhHantMsg.MIXLY_ke_BLUETOOTH='蓝牙2.0模块';
ZhHantMsg.MIXLY_ke_read='接收到信号';


//ZhHantMsg.MIXLY_ke_kzsc = '控制输出';

ZhHantMsg.MIXLY_ke_Count='灯号';

ZhHantMsg.MIXLY_ke_YEAR = '年';
ZhHantMsg.MIXLY_ke_MONTH = '月';
ZhHantMsg.MIXLY_ke_DAY = '日';
ZhHantMsg.MIXLY_ke_HOUR = '时';
ZhHantMsg.MIXLY_ke_MINUTE = '分';
ZhHantMsg.MIXLY_ke_SECOND = '秒';
ZhHantMsg.MIXLY_ke_WEEK = '周';

ZhHantMsg.MIXLY_ke_angle = '角度';

ZhHantMsg.kids_Ode_to_joy = "圣诞歌";
ZhHantMsg.kids_birthday = "生日快乐";

ZhHantMsg.kids_tone = "音调";
ZhHantMsg.kids_beat = "节拍";
ZhHantMsg.kids_play_tone = "播放乐曲";
ZhHantMsg.kids_notone1 = "关闭蜂鸣器";

ZhHantMsg.kids_ADkey = "7位按键模块";

//////////////keyestudio///////////////
ZhHantMsg.Ks_ON='高';
ZhHantMsg.Ks_OFF='低';

ZhHantMsg.ks_MQ_d='MQ-2可燃气体传感器_数字';
ZhHantMsg.ks_MQ_a='MQ-2可燃气体传感器_模拟';

ZhHantMsg.KS_LED3wd='LED_3W';
ZhHantMsg.KS_LED_W='白色LED';
ZhHantMsg.KS_LED_R='红色LED';
ZhHantMsg.KS_LED_G='绿色LED';
ZhHantMsg.KS_LED_B='蓝色LED';
ZhHantMsg.KS_LED_Y='黄色LED';
ZhHantMsg.MIXLY_KS_BUZZER1='有源蜂鳴器';
ZhHantMsg.MIXLY_KS_BUZZER2='無源蜂鳴器';
ZhHantMsg.ks_tone='音调';
ZhHantMsg.ks_beat='节拍';
ZhHantMsg.ks_music='播放乐曲';
ZhHantMsg.ks_Ode_to_joy='圣诞歌';
ZhHantMsg.ks_birthday='生日快乐';
ZhHantMsg.ks_tetris='俄罗斯方块';
ZhHantMsg.ks_star_war='星球大战';
ZhHantMsg.ks_super_mario='超级玛丽';
ZhHantMsg.ks_christmas='圣诞歌';
ZhHantMsg.ks_notone='关闭蜂鸣器';

ZhHantMsg.MIXLY_KS_RELAY='繼電器';
ZhHantMsg.MIXLY_KS_MOTOR='小风扇';
ZhHantMsg.MIXLY_KS_SERVO='舵機';
ZhHantMsg.MIXLY_KS_2812RGB='2812RGB';

ZhHantMsg.MIXLY_KS_IR_G='人體紅外傳感器';
ZhHantMsg.MIXLY_KS_FLAME='火焰传感器_数字值';
ZhHantMsg.MIXLY_KS_FLAME_a='火焰传感器_模拟值';
ZhHantMsg.MIXLY_KS_HALL='霍爾傳感器';
ZhHantMsg.MIXLY_KS_CRASH='碰撞傳感器';
ZhHantMsg.MIXLY_KS_BUTTON='按鍵';
ZhHantMsg.MIXLY_KS_TUOCH='電容觸摸';
ZhHantMsg.MIXLY_KS_KNOCK='敲擊模塊';
ZhHantMsg.MIXLY_KS_TILT='傾斜模塊';
ZhHantMsg.MIXLY_KS_SHAKE='振動模塊';
ZhHantMsg.MIXLY_KS_REED_S='幹簧管模塊';
ZhHantMsg.MIXLY_KS_TRACK='循跡模塊';
ZhHantMsg.MIXLY_KS_AVOID='避障模塊';
ZhHantMsg.MIXLY_KS_LIGHT_B='光折斷模塊';

ZhHantMsg.MIXLY_KS_ANALOG_T='模擬溫度傳感器';
ZhHantMsg.MIXLY_KS_SOUND='聲音傳感器';
ZhHantMsg.KS_LIGHT='光敏传感器';
ZhHantMsg.MIXLY_KS_WATER='水位傳感器';
ZhHantMsg.KS_SOIL='土壤湿度傳感器';
ZhHantMsg.MIXLY_KS_POTENTIOMETER='旋转電位器';
ZhHantMsg.MIXLY_KS_LM35='LM35溫度傳感器';
ZhHantMsg.MIXLY_KS_SLIDE_POTENTIOMETER='滑動電位器';
ZhHantMsg.MIXLY_KS_TEMT6000='TEMT6000環境光';
ZhHantMsg.KS_STEAM='水滴傳感器';
ZhHantMsg.MIXLY_KS_FILM_P='薄膜壓力傳感器';
ZhHantMsg.MIXLY_KS_JOYSTICK='遙桿模塊';
ZhHantMsg.MIXLY_KS_SMOKE='煙霧傳感器';
ZhHantMsg.MIXLY_KS_ALCOHOL='酒精傳感器';
ZhHantMsg.MIXLY_KS_MQ135='MQ135空氣質量';
ZhHantMsg.MIXLY_KS_18B20='18B20溫度模塊';
ZhHantMsg.MIXLY_KS_RT='获取温度';

ZhHantMsg.MIXLY_KS_DHT11='溫濕度模塊';
ZhHantMsg.MIXLY_DHT11_H='获取湿度';    /////////////
ZhHantMsg.MIXLY_DHT11_T='获取温度';     ////////////
ZhHantMsg.MIXLY_KS_BMP180='BMP180高度計模塊';
ZhHantMsg.MIXLY_KS_T='温度';
ZhHantMsg.MIXLY_KS_QY='大气压';
ZhHantMsg.MIXLY_KS_H='高度';

ZhHantMsg.KS_SR01='超聲波模塊';
ZhHantMsg.MIXLY_KS_3231='3231時鐘';
ZhHantMsg.MIXLY_KS_GET = '获取DS3231时钟时间';
ZhHantMsg.KS_ADXL345='三轴加速度计传感器_ADXL345';
ZhHantMsg.MIXLY_ADXL345_X='X轴加速度'; ///
ZhHantMsg.MIXLY_ADXL345_Y='Y轴加速度'; ///
ZhHantMsg.MIXLY_ADXL345_Z='Z轴加速度'; ///
ZhHantMsg.MIXLY_ADXL345_XA='X轴角度';  ///
ZhHantMsg.MIXLY_ADXL345_YA='Y轴角度';  ///



ZhHantMsg.MIXLY_KS_OLED='OLED模塊';
ZhHantMsg.MIXLY_SETUP='初始化';  ////////////////
ZhHantMsg.MIXLY_LCD_ADDRESS='设备地址'; /////////
ZhHantMsg.MIXLY_LCD_PRINT1='打印第一行';  /////////
ZhHantMsg.MIXLY_LCD_PRINT2='打印第二行'; ///////////
ZhHantMsg.MIXLY_LCD_ROW='在第'; /////
ZhHantMsg.MIXLY_LCD_COLUMN='行第'; /////////
ZhHantMsg.MIXLY_LCD_PRINT='列打印'; ////
ZhHantMsg.MIXLY_LCD_STAT_ON='开';  /////
ZhHantMsg.MIXLY_LCD_STAT_OFF='关';  /////
ZhHantMsg.MIXLY_LCD_STAT_CURSOR='有光标';  ////
ZhHantMsg.MIXLY_LCD_STAT_NOCURSOR='无光标';  ////
ZhHantMsg.MIXLY_LCD_STAT_BLINK='闪烁';  ////
ZhHantMsg.MIXLY_LCD_STAT_NOBLINK='不闪烁'; ///
ZhHantMsg.MIXLY_LCD_STAT_CLEAR='清屏';  ///
ZhHantMsg.MIXLY_LCD_NOBACKLIGHT='关闭背光'; ///
ZhHantMsg.MIXLY_LCD_BACKLIGHT='打开背光'; ///
ZhHantMsg.MIXLY_KS_1602LCD='IIC1602LCD';
ZhHantMsg.MIXLY_KS_2004LCD='IIC2004LCD';
ZhHantMsg.MIXLY_KS_MATRIX='8*8點陣';

ZhHantMsg.MIXLY_KS_TM1637='4位8段數碼管';
ZhHantMsg.MIXLY_KS_ws='位数';
ZhHantMsg.MIXLY_KS_value='数值';
ZhHantMsg.MIXLY_KS_begin='显示的位置';
ZhHantMsg.MIXLY_KS_fill0='是否补充0';
ZhHantMsg.MIXLY_KS_light='亮度0~7';
ZhHantMsg.MIXLY_KS_XY='显或隐';
ZhHantMsg.MIXLY_KS_L='左边';
ZhHantMsg.MIXLY_KS_R='右边';
ZhHantMsg.MIXLY_KS_MH='冒号';
ZhHantMsg.MIXLY_KS_one='第一行';
ZhHantMsg.MIXLY_KS_two='第二行';
ZhHantMsg.MIXLY_KS_three='第三行';
ZhHantMsg.MIXLY_KS_four='第四行';
ZhHantMsg.MIXLY_KS_clear='             是否清屏:';


ZhHantMsg.MIXLY_KS_IR_E='紅外發射模塊';
ZhHantMsg.MIXLY_KS_IR_R='紅外接收模塊';
ZhHantMsg.MIXLY_KS_W5100='W5100以太網模塊';
ZhHantMsg.KS_BLUETOOTH='藍牙模塊';
ZhHantMsg.MIXLY_KS_rec='接收到的信号';

ZhHantMsg.MIXLY_KS_Count='灯号';

ZhHantMsg.ks_test_V='电压传感器';
ZhHantMsg.ks_test_A='电流传感器';
ZhHantMsg.MIXLY_KS_FLAME_a='火焰传感器_模拟值';
ZhHantMsg.joys_btn='摇杆按钮';
ZhHantMsg.hwwd='非接触式红外传感器,引脚为 SDA : A4 , SCL : A5';
ZhHantMsg.color_sensor='颜色传感器';
ZhHantMsg.RED_VAL='红色值';
ZhHantMsg.GREEN_VAL='绿色值';
ZhHantMsg.BLUE_VAL='蓝色值';

ZhHantMsg.matrix16and8_init='点阵16*8初始化';
ZhHantMsg.matrix16and8='点阵16*8自定义图';
ZhHantMsg.matrix16and8_image='点阵16*8图';

ZhHantMsg.Ultraviolet='紫外线传感器';
ZhHantMsg.WaterTurbidity='水浑浊度';
ZhHantMsg.C2H5O='有害气体传感器';
ZhHantMsg.CeramicVibration='压电陶瓷震动传感器';
ZhHantMsg.DustSensor='粉尘传感器';

ZhHantMsg.MMA8452 = '三轴加速度计_MMA8452';
ZhHantMsg.SHT31 = '温湿度传感器_SHT31';

ZhHantMsg.ks_neopixel = 'RGB灯 neopixel';
ZhHantMsg.ks_neopixel_show = 'neopixel show';

ZhHantMsg.ks_TM1637_INIT ='四位数码管_TM1637 初始化';
ZhHantMsg.ks_4DIGITDISPLAY = '四位数码管_TM1637 显示';
ZhHantMsg.ks_4DIGITDISPLAY_time = '四位数码管_TM1637 显示时间';
ZhHantMsg.ks_4DIGITDISPLAY_brightness = '四位数码管_TM1637 亮度';
ZhHantMsg.ks_4DIGITDISPLAY_clean = '四位数码管_TM1637 清屏';

ZhHantMsg.ks_SPI_recv = '接收到数据 获取寄存器数据';
ZhHantMsg.ks_SPI_run = '执行';
ZhHantMsg.ks_SPI_read = 'SPI从机获取寄存器数据';

export const ZhHantCatgories = {};